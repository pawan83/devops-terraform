resource "aws_instance" "terraform-host" {

  ami           = var.AMIS[var.AWS_REGION]
  instance_type = var.instance_type
  tags = {
    Name="Terrafomr-host"
    Project="Solstice"
    }

  # the VPC subnet
  subnet_id = aws_subnet.demo1.id

  # the security group
  #vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  # the public SSH key
  #key_name = aws_key_pair.mykeypair.key_name
}
